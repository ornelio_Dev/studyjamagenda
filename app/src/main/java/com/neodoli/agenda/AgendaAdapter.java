package com.neodoli.agenda;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by DEMAUSER on 5/19/2016.
 */
public class AgendaAdapter extends BaseAdapter {

    ArrayList<ActividadeAgenda> dados=null;
    Context context=null;

    public AgendaAdapter(ArrayList<ActividadeAgenda> dados,  Context context){
        this.dados=dados;
        this.context=context;

    }
    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, parent, false);

        ActividadeAgenda actividade=dados.get(position);

        TextView data= (TextView)  rowView.findViewById(R.id.date);
        data.setText(actividade.getDate());
        TextView title= (TextView)  rowView.findViewById(R.id.title);
        title.setText(actividade.getTitle());
        TextView place= (TextView)  rowView.findViewById(R.id.place);
        place.setText(actividade.getPlace());
        return rowView;
    }
}
