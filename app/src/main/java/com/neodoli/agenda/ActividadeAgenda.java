package com.neodoli.agenda;



/**
 * Created by DEMAUSER on 5/19/2016.
 */
public class ActividadeAgenda {
    private String title="";
    private String description="";
    private String place="";
    private String date="";
    private int id;

    public ActividadeAgenda (String title, String Description, String place,String date ){
        this.title=title;
        this.description=Description;
        this.place=place;
        this.date=date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle(){
        return this.title;
    }

    public String getPlace(){
        return this.place;
    }

    public String getDescription(){
        return this.description;
    }

    public String getDate(){
        return this.date;
    }




}
